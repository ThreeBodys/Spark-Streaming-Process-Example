package MsgModel._Base;

/**
 * Created by Administrator on 2016/12/09 0009.
 */
public class _BaseMsg   {
    private String UserIp;
    private String ServerIp;
    private String From;
    private long TimeStamp;
    private long SendTimeStamp;
    public _BaseMsg() {
    }
    public _BaseMsg(String userIp, String serverIp, String from, long timeStamp, long sendTimeStamp) {
        UserIp = userIp;
        ServerIp = serverIp;
        From = from;
        TimeStamp = timeStamp;
        SendTimeStamp = sendTimeStamp;
    }



    public String getUserIp() {
        return UserIp;
    }

    public void setUserIp(String userIp) {
        UserIp = userIp;
    }

    public String getServerIp() {
        return ServerIp;
    }

    public void setServerIp(String serverIp) {
        ServerIp = serverIp;
    }

    public long getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        TimeStamp = timeStamp;
    }

    public String getFrom() {
        return From;
    }

    public void setFrom(String from) {
        From = from;
    }

    public long getSendTimeStamp() {
        return SendTimeStamp;
    }

    public void setSendTimeStamp(long sendTimeStamp) {
        SendTimeStamp = sendTimeStamp;
    }
}
