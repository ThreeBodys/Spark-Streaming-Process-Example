package MsgModel.Result;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/12/14 0014.
 */
public class BaseResultMsg implements Serializable {
    public BaseResultMsg() {
    }

    public BaseResultMsg  (String id, String result, long timeStamp, long sendTimeStamp)   {
        Id = id;
        Result = result;
        From = "SparkStreamingCalculator";
        Ip = "";
        TimeStamp = timeStamp;
        SendTimeStamp = sendTimeStamp;
    }

    private String Id;
    private String Result;
    private String From;
    private String Ip;
    private long TimeStamp;
    private long SendTimeStamp;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getFrom() {
        return From;
    }

    public void setFrom(String from) {
        From = from;
    }

    public String getIp() {
        return Ip;
    }

    public void setIp(String ip) {
        Ip = ip;
    }

    public long getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        TimeStamp = timeStamp;
    }

    public long getSendTimeStamp() {
        return SendTimeStamp;
    }

    public void setSendTimeStamp(long sendTimeStamp) {
        SendTimeStamp = sendTimeStamp;
    }
}
