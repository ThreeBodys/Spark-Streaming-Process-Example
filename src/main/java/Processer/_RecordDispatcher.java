package Processer;

import MsgModel.UserBehavior.UserBehaviorMsg;
import Util.BehaviorEnum;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import scala.Tuple2;


/**
 * Created by Administrator on 2016/12/20 0020.
 */
public class _RecordDispatcher {
    public static void Dispatch(JavaPairDStream<String, UserBehaviorMsg> messages) {

        for (BehaviorEnum e : BehaviorEnum.values()) {
            JavaPairDStream<String, UserBehaviorMsg> tempDStream = messages.filter(new Function<Tuple2<String, UserBehaviorMsg>, Boolean>() {
                @Override
                public Boolean call(Tuple2<String, UserBehaviorMsg> stringUserBehaviorMsgTuple2) throws Exception {

                    return stringUserBehaviorMsgTuple2._2().getBehavior().equalsIgnoreCase(e.toString());
                }
            });
            switch (e) {
                case Login:
                    UserProcesser.LoginCount(tempDStream);
                    break;
                case ViewPage:
                    break;
                case Query:
                    break;
            }
        }

    }
}
