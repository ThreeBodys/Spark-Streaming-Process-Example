package Util;

/**
 * Created by Administrator on 2016/12/16 0016.
 */
public enum BehaviorEnum {
    Login(1),
    ViewPage(2),
    Query(3),
    BtnClick(4);
    private int Behavior;

    BehaviorEnum(int behavior) {
        Behavior = behavior;
    }

    public String GetValue() {
        return String.valueOf(Behavior) ;
    }

}
