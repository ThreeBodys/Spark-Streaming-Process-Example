package Util;

/**
 * Created by Administrator on 2016/12/12 0012.
 */
public class Const {

    public static final String Mongo_URI_DEFAULT = "mongodb://sa:sa@192.168.1.111/Bobo_UserBehavior.UserBehaviorLog";
    public static final String BROKER_HOST = "192.168.1.111:9092";
    public static final String Mongo_Host = "192.168.1.111";
    public static final int Mongo_Port = 27017;
    public static final String Mongo_User = "sa";
    public static final String Mongo_Pwd = "sa";
    public static final String Mongo_DB = "Bobo_UserBehavior";
    public static final String SPRAK_KAFKA_COLLECTION = "SparkKafkaOffsets";
    public static final String USER_BEHAVIOR_COLLECTION = "UserBehaviorLog";
    public static final String USER_LOGINCOUNT_COLLECTION = "LoginCountLog";
    public static final String USER_PageCount_COLLECTION = "PageCountLog";

}
